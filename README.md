# Manuscript de thèse

Importé et synchronisé avec l'outil [Sharelatex](sharelatex.irisa.fr).

## Conseils de développement

### Synchronisation avec Sharelatex

Installer https://pypi.org/project/sharelatex/ de préférence dans un environement virtuel

### Suggestion de lignes à ajouter à son `.git/info/exclude`

```
.build
.vscode
.pyenv
*.aux
*.fdb_latexmk
*.flg
*.fls
*.lof
*.log
*.maf
*.mtc*
*.out
*.pdf
*.synctex.gz
*.toc
*.bbl
*.blg
```

### Configuration vscode

suggestion de lignes à ajouter à son `.vscode/settings.json` ou `settings.json` global

```json
{
    "latex-workshop.view.pdf.viewer": "tab",
    "latex-workshop.synctex.afterBuild.enabled": true,
    "files.exclude": {
        "**/.pyenv": true,
        "**/.vscode": true,
        ".build" : true,
        ".vscode" : true,
        ".pyenv" : true,
        "**/*.aux" : true,
        "*.fdb_latexmk" : true,
        "*.flg" : true,
        "*.fls" : true,
        "*.lof" : true,
        "*.log" : true,
        "*.maf" : true,
        "*.mtc*" : true,
        "*.out" : true,
        "*.pdf" : true,
        "*.synctex.gz" : true,
        "*.toc" : true,
        "*.bbl" : true,
        "*.blg" : true,
    },
    "files.watcherExclude": {
        "**/.git/objects/**": true,
        "**/.git/subtree-cache/**": true,
        "**/node_modules/**": true,
        "**/.hg/store/**": true,
        "**/.pyenv/**": true,
        ".build" : true,
        ".vscode" : true,
        ".pyenv" : true,
        "**/*.aux" : true,
        "*.fdb_latexmk" : true,
        "*.flg" : true,
        "*.fls" : true,
        "*.lof" : true,
        "*.log" : true,
        "*.maf" : true,
        "*.mtc*" : true,
        "*.out" : true,
        "*.pdf" : true,
        "*.synctex.gz" : true,
        "*.toc" : true,
        "*.bbl" : true,
        "*.blg" : true,
    },
    "search.exclude": {
        "**/.pyenv": true,
        ".build" : true,
        ".vscode" : true,
        ".pyenv" : true,
        "**/*.aux" : true,
        "*.fdb_latexmk" : true,
        "*.flg" : true,
        "*.fls" : true,
        "*.lof" : true,
        "*.log" : true,
        "*.maf" : true,
        "*.mtc*" : true,
        "*.out" : true,
        "*.pdf" : true,
        "*.synctex.gz" : true,
        "*.toc" : true,
        "*.bbl" : true,
        "*.blg" : true,
    }
}
```