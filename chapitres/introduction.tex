\chapter{Introduction}
\minitoc

\section{Préambule}

Cette thèse Cifre s'inscrit dans une collaboration de longue date entre la société Diatelic, l'Inria et le Loria. Diatélic est en effet une société Lorraine créée en 2002 par des chercheurs de l'Inria Nancy Grand Est et du Loria. Aujourd'hui, cette entreprise est une filiale du Groupe Pharmagest qui est un des grands groupes français spécialistes des infrastructures logicielles pour les pharmacies. Il y a une dizaine d'années, convaincue que les pharmaciens allaient jouer un rôle de plus en plus important dans le suivi des pathologies chroniques, la société Pharmagest a créé en son sein, un secteur e-santé destiné à répondre aux défis sociétaux que posent le vieillissement de la population ou la désertification médicale. C'est dans cet  objectif que Pharmagest  fait alors l'acquisition de Diatelic, société spécialisée dans l’intelligence artificielle appliquée à la télésurveillance et au suivi de l’observance des patients. La société Diatelic s'est d'abord fait connaître par ses solutions innovantes de télémédecine pour le  traitement de l’insuffisance rénale et le suivi de patient sous dialyse à domicile. Il s'agit d'une des premières entreprises en France à proposer un service de télémédecine opérationnel dès 2001. Plus récemment, la société Diatelic s'est orientée vers le développement de solutions pour le maintien à domicile des personnes âgées en perte d'autonomie, afin de permettre aux personnes âgées de rester chez elles plus longtemps qu'elles ne peuvent le faire actuellement.

%% de télé-médecine pour les personnes fragiles et d'aide au diagnostique, au suivi médical et à la prise de décision auprès des professionnels de santé. Diatélic est une filiale du groupe Pharmagest qui produit principalement des solutions logiciels pour les pharmacies mais qui s'étend pour inclure aussi des solutions de télé-médecine à domicile basé sur une boxe et des capteurs ambiants qui s'installent sur les murs et portes.

De manière  concomitante, l'équipe Maia puis l'équipe Larsen communes  à l'Inria et au Loria, ont développé un projet scientifique autour de la robotique et de l'intelligence artificielle dont une des applications est la conception de solutions qui facilitent le maintien à domicile des personnes en situation de fragilité, en perte d’autonomie ou dépendantes. Ce domaine d'application a vu le jour en 2010 suite à différentes incitations. Tout d'abord, la région Lorraine a inscrit dans son plan stratégique la Silver économie parmi l'un des axes qu'elle souhaitait soutenir. Ainsi la Lorraine et aujourd'hui la région Grand Est ont fortement financé différentes opérations autour des Contrats de plan État-Région (CPERs) et de projets d'Appel à Manifestation d'Intérêt (AMI) avec Pharmagest. L'Inria a financé d'autre part un projet d'envergure National PAL (personal Assisted Living), ce qui fait qu'aujourd'hui l'équipe Larsen a pu développer une activité de recherche soutenue sur l'assistance à la personne.
Les solutions s’appuient sur différentes technologies à l'interface  entre intelligence artificielle, robotique et domotique. L’équipe dispose d’une plateforme de recherche: \emph{l’Habitat intelligent pour la santé}. Il s’agit d’un lieu dédié à l’expérimentation comprenant 4 pièces meublées, 700 capteurs (caméras de profondeur, dalles intelligentes, capteurs de présence, microphones, etc.) et des robots mobiles. Cette plateforme d’expérimentation offre un cadre idéal pour inventer les technologies de demain en matière d’aide à l’autonomie et de maintien à domicile des personnes fragiles ou dépendantes.


\section{Contexte}


\subsection{Démographie et population senior}

% on a l'impression que je chie sur le fait qu'on vie plus longtemp...
% alors, on a envie envie de prendre sa retraite à 65 ans ?

Le vieillissement de la population confronte les sociétés contemporaines à une transformation démographique sans précédent.

D'après la base de données de la Banque mondiale,
le ratio de dépendance des personnes âgées, qui est le rapport entre
le nombre de personnes âgées de plus de 64 ans et la population en âge de travailler
est passé de 8,6\% en 1960 à 13,94\% en 2019 \citep{age-dependency-ratio} sur la population mondiale, de 19\% en 1960 à 33\% en France.
%TODO: montrer la courbe et peut etre la carte aussi dans une figure
Selon le bureau du recensement des États-Unis par exemple, environ 29\% des plus de 65 ans vivent seuls \citep{demographics} avec un coût médian national d'une chambre semi-privée en maison de retraite qui devrait croître de 243\% entre 2017 et 2047 \citep{cost-care-2017}.

En France, selon les projections de l’INSEE (Insee Première $N^o1089$ - juillet 2006), en 2050, près d’un habitant sur trois aura plus de 60 ans, contre un sur cinq en 2005. Ce ratio aura presque doublé en 45 ans. Une telle évolution pose, au-delà du problème du financement des retraites et de la dépendance, un allongement de la durée de vie qui ne fera qu'accroître le nombre de personnes souffrant soit d’une perte d’autonomie soit de maladies chroniques. Cela va fortement impacter l’organisation de notre système de santé. Déjà aujourd'hui l'impact des maladies chroniques et de la dépendance est considérable : 15 à 20 millions de personnes en France sont concernées. Le coût total de ces maladies est très élevé et augmente avec l’accroissement de la demande de soins.
La perte d’autonomie touche de nombreuses familles nécessitant le placement en institution des personnes concernées. Par ailleurs, la démographie médicale et paramédicale diminue quantitativement et sa répartition sur le territoire n'est pas uniforme. En d'autres termes, l’offre de soin tend à se raréfier, et ce de manière non homogène, alors que la demande va considérablement augmenter dans les années qui viennent. En effet \citet{baisse-effectif} constatent dans leur étude une baisse de l'offre globale de soins d'une ampleur plus importante que celle des effectifs qui, mise en parallèle avec le vieillissement de la population, fait que l'offre médicale devrait croître moins vite que la demande, au cours des dix prochaines années, c.-à-d. à l'horizon 2027.
Pour faire face à cette situation, les progrès des technologies de l’information et de la communication permettent le développement de nouvelles solutions de prise en charge des personnes qui souffrent de pathologies chroniques graves ou d’un vieillissement nécessitant un suivi. Ces nouvelles solutions vont radicalement changer l’organisation des soins en particulier parce qu'elles permettent d’augmenter de manière significative la prise en charge à domicile des personnes. Cette dernière est source de confort pour les patients et est souvent beaucoup plus pertinente comme l’a démontré la société Diatelic dans les domaines de l’insuffisance rénale. Au-delà de l’insuffisance rénale, sont aussi concernées les maladies chroniques comme l’insuffisance cardiaque, l’insuffisance respiratoire, le diabète, l’hypertension et les problèmes liés au sommeil.
Par ailleurs, les personnes âgées vivant seules à domicile sont sujettes à des risques du type chutes, baisse d’activité ou comportements pathologiques divers. La possibilité de détecter ces problèmes et d’intervenir rapidement grâce à des dispositifs et à des services innovants est prometteuse dans l'optique de permettre aux personnes âgées ou en situation de handicap de vivre plus facilement et plus longtemps dans leur logement en profitant de leur environnement social.

\subsection{Habitat intelligent pour le maintien à domicile}

Une des réponses à cette problématique sociétale du vieillissement de la population est le développement de technologies qui facilitent le maintien à domicile des personnes âgées.

L’état de l’art du domaine regorge de projets qui s'intéressent à cette question. Parmi eux, beaucoup consistent à développer des systèmes de télésurveillance à domicile, on parle aussi souvent dans la littérature d'habitats intelligents ou d'habitat connectés. L'objectif dans ce cadre est d'équiper un logement de capteurs afin de détecter, voire de prévenir l'occurrence de situations inquiétantes ou critiques et d'évaluer l'état physique et la fragilité des personnes suivies. Différents types de capteurs peuvent être installés à domicile : des capteurs environnementaux ou des capteurs portés par la personne. Les premiers sont souvent privilégiés, car ils sont intégrés au lieu de vie et sont autonomes, les personnes ne sont donc pas sollicitées pour les recharger ou à les porter. Les seconds peuvent aussi être envisagés : parmi ceux-ci il y a par exemple les montres connectées qui peuvent relever l'activité des personnes équipées. Un avantage de ces capteurs portés est qu'ils sont forcément associés à une personne donnée ce qui n'est pas le cas des capteurs environnementaux qui eux posent une problématique lorsque plusieurs locataires coexistent au sein du même logement. Lorsque l'on veut équiper des logements à large échelle, se pose également la question du coût de l'installation qui peut fortement varier en fonction du type de capteur choisi, et leur nombre.

D'autres projets s'intéressent au rôle que pourraient jouer les robots dans la téléassistance et l'interaction, ainsi que l'acceptabilité des personnes vis-à-vis de ce type d'outils dans leur environnement de vie.




% \begin{itemize}
% \item Problématique à différentes facettes: technique(installations, capteurs, robots), organisation(EPADs, lois), social/sociétal(acceptabilité, démographie, meurs de la société), scientifique(algorithmes, modèles, fusion), médical (suivis, prescription)
% \end{itemize}

\subsection{Apprentissage automatique et suivi des habitudes de vie}

L'état de l'art de la recherche en assistance à l'autonomie à domicile repose en majeure partie sur des technologies dites d'«intelligence ambiante». Il s'agit d'un paradigme où les personnes sont assistées et guidées dans leurs activités quotidiennes par des systèmes capables de suivi, d'anticipation et de conseil tout en étant non-invasifs. Ces services d'assistance à l'autonomie à domicile englobent différentes technologies comprenant capteurs, effecteurs, boîtiers de communication et interfaces. Ils servent aussi différents tiers d'utilisateurs dont les demandes et attentes seront tout aussi variées.

% \subsection{Problématiques et axes de recherche}

% L'industrie de la domotique, l'internet des objets et les capteurs portatifs faisant coach de sport et santé connaît un essor sans précédent ces dernières années. Ce marché émergeant à vu un afflue de sociétés voulant surfer sur la vague de cette nouvelle mode sortant et proposant tout les mois des produits 

% \subsection{Activités de la Vie Quotidienne}

% \begin{itemize}
% \item Introduction du formalisme d'ADL qui va être detaillé dans le chapitre sur l'etat de l'art
% \item Pourquoi on explore cette piste de formalisme de activité quotidienne pour faire le suivi de l'état de santé (en quoi ça va «résoudre notre problème» ?)
% \end{itemize}

\section{Objectif de la thèse}

La société Diatelic développe actuellement une offre de télésurveillance à destination des personnes âgées et des personnes fragiles. Cette offre s'articule autour d'une Box connectée et un ensemble de capteurs d'activité. Autour de cette offre matérielle, Diatélic propose un ensemble de services d'analyse et d'interprétation des données. C'est ici que se positionne la thèse dont la contribution est la recherche et développement d'algorithmes capables d’apprendre les habitudes de vie pour en déduire des incohérences qui peuvent potentiellement révéler l'apparition d'une fragilité.
La solution doit donc assurer un suivi personnalisé et améliorer la coordination des intervenants, que ce soit à leur domicile ou en maison de retraite médicalisée ou non. Elle doit permettre également de maintenir le lien avec la famille et les aidants grâce à sa connexion à un réseau social dédié. Ce travail rentre dans le cadre du projet « 36 mois de plus à domicile » développé par le Groupe Pharmagest avec le soutien du Conseil Régional Grand Est et du FEDER (Fonds Européens de Développement Régional). L'objectif est d'explorer et de développer des solutions pour le suivi de résidents seniors dans leurs habitats à travers des données rapportées par des capteurs discrets, peu coûteux et non invasifs.


Plus précisément, cette thèse a pour objectif de proposer des solutions pour pouvoir évaluer le niveau d'autonomie d'une personne fragile en relevant  ce qu'on appelle  les Activités de la Vie Quotidienne (AVQ) (par exemple le sommeil, la visite de salle de bain, la préparation de repas, etc.). Cette évaluation se fera à travers un bilan automatique journalier comprenant la fréquence, durée et moment de la journée des activités relevées. Ce bilan sera envoyé au personnel soignant et au surveillant de la personne. Elle doit aussi pouvoir se faire sans aucune nécessité de mobiliser ou d'interférer en quoique ce soit avec la vie habituelle que la personne mènerait sans un tel dispositif de suivi.


L'automatisation du relevé des AVQ est une problématique de recherche qui mobilise de nombreux chercheurs à travers le monde. La question qui se pose est de savoir comment reconnaître les activités de la vie quotidienne à partir des données issues de capteurs installés à domicile. Une difficulté majeure est que, souvent pour des raisons de coût, les capteurs sont en nombres limités et relativement peu informant (capteurs binaires le plus souvent).

Ces solutions se concrétisent principalement en différents modèles et algorithmes d'inférence des activités de la vie quotidienne en partant des données capteurs. Le cœur de la thèse est de démontrer la faisabilité de la mise en place d'algorithmes d'apprentissage non supervisé pour le suivi d'activité chez les personnes équipées. Et la visée est le développement d'une solution commerciale clé en main capable de fonctionner immédiatement après son installation.

Pour la recherche et le développement de ces solutions, nous nous appuyons sur des bases de données académiques ainsi que des données réelles recueillies sur le terrain. L'avantage est que nous avons un retour immédiat des besoins et remarques du personnel soignant au fur et à mesure de l'avancée des travaux.

\subsection{Positionnement}

Le cadre des travaux se situe au sein du projet \emph{«36 mois de plus»} de Diatélic. Le but est de proposer une solution de télésurveillance facile à mettre en place et de maintenir avec des rapports immédiats après installation. Ceci impose certaines restrictions sur la nature et les propriétés des données collectées par les capteurs (nature, précision et fréquence des données). Ainsi, les algorithmes que nous proposons ici utilisent principalement les données événementielles issues de capteurs de mouvement environnementaux.

\subsection{Approche}

En raison de la pauvreté des données dont nous disposons en amont pour effectuer des inférences, et des contraintes imposées par la spécification de la solution de télésurveillance, la classe d'algorithmes que nous avons explorés devait y être tout aussi adaptée:

\begin{description}
\item[Non supervisé: ] Nous aurons tendance à préférer les algorithmes non supervisés qui n'ont pas impérativement besoin d'une période d'entrainement avant de pouvoir évaluer et retourner des résultats.
\item[Modèles: ] Nous aurons ainsi besoin d'adopter une approche principalement articulée sur des modèles.
\item[Activités: ] Les propriétés temporelles des capteurs imposent une limite sur les types d'activités possiblement inférées.
\end{description}

\subsection{Organisation du mémoire}

Dans ce travail de thèse, nous commençons par présenter un état de l'art des différentes techniques et technologies utilisées dans la littérature dans le cadre du suivi d'activité : La corrélation entre l'état de santé des personnes, leur degré d'autonomie et leurs activités de la vie quotidienne. Les outils de détection de ces activités, c.-à-d. les différents types de capteurs et données que l'on peut récupérer sur la personne suivie. Les différents algorithmes développés et employés pour inférer et prédire ces activités en se basant sur ces mêmes données. Et enfin une revue des outils et plates-formes logiciels les plus répandus dans ce milieu.

Étant donné qu'il s'agit d'une thèse Cifre, le but est d'intégrer et déployer les contributions recherchées et développées dans un cadre industriel bien précis. En considérant tout un lot de plates-formes et de services et surtout des contraintes particulières qui vont orienter les sujets abordés. Nous allons donc brièvement présenter les différents services fournis aux patients, leurs structures, les capteurs utilisés avec leur nomenclature de données et les connaissances \textit{a priori}.

S'en suivront nos deux contributions. La première concerne l'analyse et l'application de méthodes de segmentation et de classification de séries temporelles pour l'inférence d'AVQ avec un exemple sur la détection des phases de sommeil d'un résident. Nous abordons les choix et les compromis sur les approches étudiées ainsi que les potentielles évolutions et améliorations qu'offre chaque approche.

La deuxième présente une méthode de présentation et de visualisation des habitudes de vies d'un patient permettant à un expert de formuler un avis informé sur l'état de son rythme de vie. Nous y présentons la technique élaborée ainsi que plusieurs cas d'études ou nous montrons les lectures possibles offertes par cette technique.

L'apport de ce travail de thèse se situe à la fois sur le plan méthodologique et applicatif. Sur le plan méthodologique, nous proposons différentes techniques d'inférence explorées en se basant sur la structure et les données disponibles sur le terrain. Sur le plan applicatif, tout le travail effectué a été continuellement intégré à l'infrastructure existante et déployée sur le terrain avec tout le travail d'ingénierie qui en incombe.